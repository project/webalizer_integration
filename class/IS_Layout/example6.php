<?php
require_once "class.IS_Layout.php";

$lay  = new IS_Layout('example6.htm');
$slay = new IS_Layout('example6.1.htm');

$month=date("m");
$day=date("d");
$year=date("Y");

$slay->replace('month',$month);
$slay->replace('day',$day);
$slay->replace('year',$year);

// Includes the object $slay in $lay
$lay->inc($slay);

$lay->display();

?>