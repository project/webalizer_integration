<?php
require_once "class.IS_Layout.php";

$html=<<<END
<html>
<body>

<p align="center">&nbsp;</p>
<p align="center"><b>Today</b>: [month]/[day]/[year]</p>

</body>
</html>
END;

$month=date("m");
$day=date("d");
$year=date("Y");

$lay=new IS_Layout($html);

$lay->replace('month',$month);
$lay->replace('day',$day);
$lay->replace('year',$year);

$lay->display();
?>