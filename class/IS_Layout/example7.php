<?php

include_once('class.IS_Layout.php') ;

$l = new IS_Layout('example7.html') ;

$x = array(0, 1, 2) ;
$y =
    array(
        array(10, 11, 12),
        array(20, 21, 22),
        array(30, 31, 32)) ;

$l->loop_replace('row', $x) ;

foreach ($x as $a)
{
    $id = sprintf('column%d', $a) ;
    $l->loop_replace($id, $y[$a]) ;

}

echo $l->toRawHtml() ;

?>
