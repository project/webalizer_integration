<?php
require_once "class.IS_Layout.php";

$html=<<<END
<p align="center">&nbsp;</p>
<p align="center">IS_LAYOUT EXAMPLE</p>
<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1">
  <tr>
    <td width="100%" colspan="3" bgcolor="#808000">
    <p align="center"><b><font color="#FFFFFF">Friends</font></b></td>
  </tr>
  <tr>
    <td width="33%" bgcolor="#409090" align="center">Name</td>
    <td width="33%" bgcolor="#409090" align="center">Country</td>
    <td width="34%" bgcolor="#409090" align="center">Age</td>
  </tr>

  <!-- replace id="Friends" -->
  <tr>
    <td width="33%" align="center">[name]</td>
    <td width="33%" align="center">[country]</td>
    <td width="34%" align="center">[age] Years Old</td>
  </tr>
  <!-- end replace -->
  
</table>
END;

$array=array(
             array('name'=>'Paul',
                   'country'=>'Japan',
				   'age'=>48,
				   ),

             array('name'=>'John',
                   'country'=>'France',
				   'age'=>21,
				   ),

             array('name'=>'Karen',
                   'country'=>'South Korea',
				   'age'=>31,
				   )
);

$lay=new IS_Layout($html);

// Transfer the $arrray to html code
$lay->loop_replace('Friends',$array);

$lay->display();
?>