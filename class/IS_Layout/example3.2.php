<?php
require_once "class.IS_Layout.php";

// The HTML file
$html=<<<END
<html>
<body>

<p align="center">&nbsp;</p>
<p align="center"><b>Today</b>: [date,today,month]/[date,today,day]/[date,today,year]</p>
<p align="center"><b>Yesterday</b>: [date,yesterday,month]/[date,yesterday,day]/[date,yesterday,year]</p>

</body>
</html>
END;

$date['today']['month']=date("m");
$date['today']['day']=date("d");
$date['today']['year']=date("Y");

$unixyesterday=date("U")-60*60*24;
$arrayYesterday=getdate($unixyesterday);

$date['yesterday']['month']=$arrayYesterday['mon'];
$date['yesterday']['day']=$arrayYesterday['mday'];
$date['yesterday']['year']=$arrayYesterday['year'];


$lay=new IS_Layout($html);

// First Argument in replace function are ever the reference of variable in HTMl file
$lay->replace('date',$date);

$lay->display();
?>