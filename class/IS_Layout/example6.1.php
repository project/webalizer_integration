<?php
require_once "class.IS_Layout.php";

$lay  = new IS_Layout('example6.htm');
$slay = new IS_Layout('example6.1.htm');
// Includes the object $slay in $lay
$lay->inc($slay);

$month=date("m");
$day=date("d");
$year=date("Y");

$lay->replace('month',$month);
$lay->replace('day',$day);
$lay->replace('year',$year);

$lay->display();

?>