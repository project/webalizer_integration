<?php
require_once "class.IS_Layout.php";

$html=<<<END
<html>
<body>

<p align="center">&nbsp;</p>
<p align="center"><b>Today</b>: [date,month]/[date,day]/[date,year]</p>

</body>
</html>
END;

$date['month']=date("m");
$date['day']=date("d");
$date['year']=date("Y");

$lay=new IS_Layout($html);

// First Argument in replace function are ever the reference of variable in HTMl file
$lay->replace('date',$date);

$lay->display();
?>