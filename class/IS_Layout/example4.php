<?php
require_once "class.IS_Layout.php";

$html=<<<END
<html>
<body>

<p align="center">&nbsp;</p>

<!-- code id="bold" -->
	<p align="center"><b><font face="Verdana">Bold Text</font></b></p>
<!-- end code -->

<p align="center">&nbsp;</p>
<p align="center"><font face="Verdana" size="1"><b><a href="?a=change">Change</a></b></font></p>

</body>
</html>
END;

$rep=<<<END
<html>
<body>
<!-- code id="italic" -->
	<p align="center"><i><font face="Verdana">Italic Text</font></i></p>
<!-- end code -->
</body>
</html>
END;

$layBold   = new IS_Layout($html);
$layItalic = new IS_Layout($rep);

// Get the code named "italic" from $rep
$italic=$layItalic->getCode('italic');
$htmlItalic=$italic->toHtml();

// Replace the code named bold by $htmlItalic
$layBold->code_replace('bold',$htmlItalic);

$layBold->display();
?>