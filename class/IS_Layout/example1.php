<?php
require_once "class.IS_Layout.php";

// See the example1.htm for better understanding
$lay = new IS_Layout('example1.htm');

// Another way to load an html file:
// $lay = new IS_Layout;
// if(!$lay->openFile('example1.htm'))
//		echo 'Error openning the file'
//else
//		echo 'File succesfully opened';

$array=array(
array(1,2,3),
array(4,5,6),
array(7,8,9)
);

// Transfer the $arrray to html code
$lay->loop_replace('identification',$array);

$lay->display();

?>