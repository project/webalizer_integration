# !/bin/bash
#

cd `dirname $0`
home=`pwd`
webalizer=/usr/bin/webalizer

theFiles=`echo ../logs/*`

theCount=`echo $theFiles | wc -w | sed 's/ //g' `

for aFile in $theFiles; do
    if [[ $theCount > 1 ]]; then
	echo 'Processing: '$aFile
	$webalizer -p -c $home/../conf/webalizer.conf $aFile
	cp $aFile ../processed/
	rm -rf $aFile
#	svn add processed/$aFile
#	svn commit -m 'Add log file' processed/$aFile
    fi
    theCount=$(( theCount - 1 ))
done

#
# Webalizer doesn't display data older than 1 year so there
# isn't any point in keeping logs that are older than one
# year.
#

find ../processed -ctime 366 -exec rm -v \{} \;

